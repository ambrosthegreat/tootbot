# tootear

___03/04/2009___
Bot para Mastodon que responde a comandos básicos.
 - Opción de ayuda <h>.
 - Listado de mis bots <b>
 - Impulso básico <i>.

___04/04/2020___
El bot ya sólo responde a solicitudes de humanos

___16/04/2020___
Añadido a la mega_app con las siguientes funciones:

        + Listado de mis bots
        + Recibir un insulto
        + Recibir un piropo
        + Tootear lo que se encuentre a continuación del comando
        + Se puede hacer que las respuestas del bot vayan a un destinatario determinado

___18/04/2020___
Incorporado a tootbot:

        + Comando <y> que tootea un tuit de historia

___02/08/2020___
Vuelvo a sacar el bot de mega_app; mientras ha estado se le ha incorporado:
	
	+ Comando <w> para ver el tiempo en una ciudad

___10/08/2020___
Bot adaptado a la versión 3 de pyown
Incorporado a tootbot:

        + Comando <f> para pronóstico del tiempo en las próximas horas
	+ Comando <ff> para pronóstico del tiempo a 5 días

___12/08/2020___
Incorporado a tootbot:
	
	+ Comando <j> para hacer un faketuit. Dentro del mismo se pasan los datos con:
		<u_ usuario> Usuario de Tuiter del fake
		<f num> Número de favs
		<r num> Número de retuits
		<t texto> Tiempo desde el mensaje
		<c num> Número de comentarios
		El texto del fake se pasa en el cuerpo del toot

___13/08/2020___
Se añade al fake tuit la posibilidad de poner una foto. Además, si se solicita sin usuario te envía una ayuda.

___20/08/2020___
Añadido comando a petición de Julia que sólo le responde a ella.
