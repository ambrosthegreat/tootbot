#!/usr/bin/env python3
#
# run_clima
# Obtener en modo local para pruebas el clima en una localidad
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
MAX_CAR = 500

# LIBRERIAS
from argparse import ArgumentParser
from mi_clima import Mi_clima
from os import getcwd

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Corre localmente el módulo para el clima' )
    parser.add_argument( '-c', '--ciudad', help = 'Ciudad a buscar', type = str, required = True )
    parser.add_argument( '-w', '--weather_token', help = 'Token de OpenWeatherMap', type = str, required = True )
    return parser.parse_args()

def acortar( texto ):
    if len( texto ) > MAX_CAR:
        texto = texto[ 0 : 500 ]
        lineas = texto.splitlines()
        texto = ''
        for i in range( len( lineas ) - 1 ):
            texto = texto + lineas[i] + '\n'
        texto = texto[ 0 : len( texto ) - 1 ]
    return texto

def mensaje_pronostico( el_clima, ciudad ):
    print( 'mensaje_pronostico() --> ciudad: ' + str( ciudad ) )
    texto = el_clima.pronostico( ciudad )
    print( texto )

def varios_mensajes( texto ):
    mensajes = []
    while len( texto ) > 0:
        mensaje = acortar( texto )
        mensajes.append( mensaje )
        texto = texto[ len( mensaje ) : len( texto ) ]
    return mensajes

# MAIN
args = _args()

el_clima = Mi_clima( args.weather_token )
mensaje_pronostico( el_clima, args.ciudad )

print( '\n\n------------------------------------\nACORTADO:' )
print( acortar( el_clima.pronostico( args.ciudad ) ) )

mensajes = varios_mensajes( el_clima.pronostico( args.ciudad ) )
print( '\n\n------------------------------------\nVARIOS:' )
for mensa in mensajes:
    print( '\nMENSAJE:' )
    print( mensa )
