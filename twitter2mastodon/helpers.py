#!/usr/bin/env python3
import datetime
from pathlib import Path
import sys

def _read_file(path):
    """ Read file if it exists, and False on error.

    Arguments:
    path {string} -- Path to file.
    """

    file = Path(path)
    if not file.is_file():

        return False

    try:

        file = open(path)
        data = file.read()
        file.close()
    
    except Exception as e:

        print("Exception reading file.")
        print(e)

    return data

def _write_file(path, data):
    """ Write data to file, overwriting existing file if it exists. Return False on error.

    Arguments:
    path {string} -- Path to file.
    data {string} -- Content to write.
    """

    try:

        file = open(path, mode="w")
        file.write(data)
        file.close()

    except Exception as e:

        print('Exception writing file.')
        print(e)

        return False

    return True
