#!/usr/bin/env python3
#
# toot_bot
# Bot multifunción para ir agregando lo que se me ocurra
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
INST_URL = 'https://botsin.space/'  # URL de la instancia para los bots
MAX_NOTIF = 50                      # Máximo número de notificaciones guardadas como procesadas
T_ESPERA = 10                       # Tiempo de espera entre comprobación de evento

# LIBRERIAS
from argparse import ArgumentParser
from mi_clima import Mi_clima
from mi_dropbox import Mi_dropbox
from mi_ficheros import Mi_ficheros
from mi_masto import Mi_masto
from mi_tiempo import mi_hora
from os import getcwd
from time import sleep
from tootbot import comandos

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica frases de un fichero de texto' )
    parser.add_argument( '-d', '--dropbox_token', help = 'Token de acceso a Dropbox para la app', type = str, required = True )
    parser.add_argument( '-t', '--app_token_tootbot', help = 'Token de la app para el automatizador', type = str, required = True )
    parser.add_argument( '-w', '--weather_token', help = 'Token de OpenWeatherMap', type = str, required = True )
    return parser.parse_args()

def _descargar( dbx, nomfich ):
    # Descargar un fichero de Dropbox y devolver el nombre con el path
    fich = Mi_ficheros( nomfich )
    if not( dbx.descargar( fich.en_path(), fich.en_raiz() ) ):
        fich.crear_fichero()
    return fich.en_path()

def _existe_dato_en( mi_dato, lista ):
    # Devuelve un boleano en función de si mi_dato está o no dentro de lista
    existe_dato = False
    for dato in lista:
        if dato == mi_dato:
            existe_dato = True
    return existe_dato

def _cargar( nomfich, lista, dbx ):
    # Guarda la lista en un fichero y lo sube a dropbox
    f = Mi_ficheros( nomfich )
    f.escribe_lista( lista )
    dbx.cargar( f.en_path(), f.en_raiz() )

def _notif_procesada( notif, nomfich, dbx ):
    # Devuelve True si la notificación está procesada. En caso contrario, la mete en el fichero de procesadas y devuelve False.
    procesada = False

    fich = Mi_ficheros( nomfich )
    lst_notif_proc = fich.lee_lineas()
    if _existe_dato_en( notif.id, lst_notif_proc ):
        procesada = True
    else:
        if len( lst_notif_proc ) >= MAX_NOTIF:
            lst_salida = []
            for indice in range( 1, MAX_NOTIF ):
                lst_salida.append( lst_notif_proc[indice] )
        else:
            lst_salida = lst_notif_proc

        lst_salida.append( notif.id )
        _cargar( nomfich, lst_salida, dbx )
    return procesada

# MAIN
args = _args()
hora = mi_hora()

masto = Mi_masto( INST_URL, args.app_token_tootbot )
dbx = Mi_dropbox( args.dropbox_token )
el_clima = Mi_clima( args.weather_token )

f_insultos = _descargar( dbx, 'insultos.txt' )
f_piropos = _descargar( dbx, 'piropos.txt' )
f_hist = _descargar( dbx, 'twitter_historia.nfo' )
f_notif = _descargar( dbx, 'notificaciones.nfo' )
f_julia = _descargar( dbx, 'Julia.txt' )

while True:
    notifs = masto.comprueba_notif( T_ESPERA )
    for notif in notifs:
        if not( _notif_procesada( notif, f_notif, dbx ) ):
            comandos( masto, notif, f_insultos, f_piropos, el_clima, f_hist, f_julia )
    sleep( T_ESPERA )
