#!/usr/bin/env python3
#
# https://openweathermap.org/
# Documentación PyOWM -> https://pyowm.readthedocs.io/en/latest/

# CONSTANTES

# LIBRERÍAS
from datetime import datetime
from mi_tiempo import a_zona
from pyowm.utils.config import get_default_config
from pyowm import OWM
from requests import get as get_url

# FUNCIONES
class Mi_clima():
    def __init__( self, mi_token ):
        try:
            mi_config_dict = get_default_config()
            mi_config_dict['language'] = 'es'
            self.owm = OWM( mi_token, mi_config_dict )
            self.token = mi_token
            print( 'mi_clima() => Conectado' )
        except:
            print( 'mi_clima() => Error al conectar' )

    def _descomponer( self, cadena ):
        # Divide cadena en ciudad y país
        if cadena.find( ',' )>0:
            pais = cadena[ cadena.find( ',' ) + 1 : len( cadena ) ].strip()
            pais = pais.upper()
            if len( pais ) > 2:
                pais = pais[ 0 : 1 ]
            elif len( pais ) < 2:
                pais = ''
            ciudad = cadena[ 0 : cadena.find( ',' ) ].strip()
        else:
            ciudad = cadena
            pais = ''

        return ciudad, pais

    def _obtener_id( self, ciudad_pais ):
        # Obtener ID de la ciudad
        # [(3104324, 'Zaragoza', 'ES'), (6362983, 'Zaragoza', 'ES')]
        ciudad, pais = self._descomponer( ciudad_pais )

        if ciudad.isdigit():
            ciudad_id = int( ciudad )
        else:
            # [(3104324, 'Zaragoza', 'ES'), (6362983, 'Zaragoza', 'ES')]
            try:
                reg = self.owm.city_id_registry()
                if pais == '':
                    lista_ids = reg.ids_for( ciudad )
                else:
                    lista_ids = reg.ids_for( ciudad, country=pais )
                print( '_obtener_id()--> Lista IDs: ' + str( lista_ids ) )
                if len( lista_ids ) == 1:
                    ciudad_id = lista_ids[0][0]
                else: 
                    ciudad_id = lista_ids
            except:
                ciudad_id = None

        return ciudad_id

    def _datos_a_texto( self, mi_weather ):
        # Obtiene los datos del tiempo en una ciudad dada por su ID y los devuelve en una cadena de texto para imprimir
        cadena = 'Estado: ' + str( mi_weather.detailed_status )
        # {'temp': 23.0, 'temp_max': 23.0, 'temp_min': 23.0, 'feels_like': 24.33, 'temp_kf': None} { 'kelvin', 'celsius', 'fahrenheit' }
        cadena = cadena + '\nTemperatura: ' + str( mi_weather.temperature( 'celsius' )['temp'] ) + 'ºC'
        cadena = cadena + '\nMín/máx día: ' + str( mi_weather.temperature( 'celsius' )['temp_min'] ) + '/' + str( mi_weather.temperature( 'celsius' )['temp_max'] ) + 'ºC'
        cadena = cadena + '\nPresión: ' + str( mi_weather.pressure['press'] ) + 'mmHg'
        cadena = cadena + '\nHumedad: ' + str( mi_weather.humidity ) + '%'
        cadena = cadena + '\nViento: ' + str( round( mi_weather.wind()['speed'] * 3.6, 2 ) ) + 'km/h' # Se puede hacer con pyowm.utils.measurables.metric_wind_dict_to_km_h(d)
        cadena = cadena + '\nNubosidad: ' + str( mi_weather.clouds ) + '%'
        if ( len( mi_weather.rain ) ) > 0:
            cadena = cadena + '\nLluvia: ' + str( mi_weather.rain['1h'] ) + 'mm/1h'
        if ( len( mi_weather.snow ) ) > 0:
            cadena = cadena + '\nNieve: ' + str( mi_weather.snow['1h'] ) + 'mm/1h'

        return cadena

    def datos_api_2_5( self, city_id ):
        # En la API 3.0.0 no he encontrado la manera de extraer el nombre de la ciudad a partir de su ID
        # Para no instalar APIs de diferentes versiones hago una consulta a la web
        # http://api.openweathermap.org/data/2.5/weather?id=6360720&appid=1cc1430e3405832ae2e1915f0b65a738
        # {"coord":{"lon":-3.82,"lat":43.47},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"base":"stations","main":{"temp":294.04,"feels_like":297.47,"temp_min":293.71,"temp_max":294.26,"pressure":1016,"humidity":100},"visibility":10000,"wind":{"speed":1,"deg":0},"clouds":{"all":75},"dt":1596959185,"sys":{"type":1,"id":6441,"country":"ES","sunrise":1596950037,"sunset":1597001257},"timezone":7200,"id":6360720,"name":"Santander","cod":200}
        la_url = 'http://api.openweathermap.org/data/2.5/weather'
        la_url = la_url + '?id=' + str( city_id )
        la_url = la_url + '&appid=' + str( self.token )
        resultado = get_url( la_url ).content
        return resultado

    def _str_ciudad( self, ciudad_id ):
        # Devuelve una cadena con la ciudad y país a partir de una consulta a la web en la API 2.5
        datos = str( self.datos_api_2_5( ciudad_id ) )
        ciudad = datos.split( '\"name\":\"' )[-1]
        ciudad = ciudad.split( '\"' )[0]
        pais = datos.split( '\"country\":\"' )[-1]
        pais = pais.split( '\"' )[0]
        longitud = datos.split( '\"lon\":' )[-1]
        longitud = longitud.split( ',' )[0]
        latitud = datos.split( ',\"lat\":' )[-1]
        latitud = latitud.split( '}' )[0]

        salida = ciudad + ', ' + pais  + ' (' + latitud + ', ' + longitud + ')'
        return salida

    def tiempo_ahora( self, ciudad_pais ):
        # Devuelve el tiempo en una ciudad determinada
        #Inicialización
        texto = 'Ciudad no encontrada'
        url_icono = 'https://image.flaticon.com/icons/png/512/1207/1207711.png'

        ciudad_id = self._obtener_id( ciudad_pais )
        if ciudad_id != None:
            if type( ciudad_id ) == int:
                mgr = self.owm.weather_manager()
                try:
                    w = mgr.weather_at_id( ciudad_id ).weather
                except:
                    # Si hay error es porque ciudad_id no corresponde con ninguna ciudad
                    ciudad_id = None
    
                if ciudad_id != None:
                    texto = 'Ciudad: ' + self._str_ciudad( ciudad_id ) + '\n\n' + self._datos_a_texto( w )
                    url_icono = w.weather_icon_url()
                    print( 'tiempo_ahora() --> Icono = ' + str( url_icono ) )
            else:
                texto = 'Ciudad ambigüa; opciones:\n'
                for desambigua in ciudad_id:
                    texto = texto + '\n' + str( desambigua[0] ) + str( '  ' ) + self._str_ciudad( desambigua[0] )

        salida = { 'texto':texto, 'url_icono':url_icono }
        return salida

    def pronostico( self, ciudad_pais ):
        # Devuelve el pronóstico a 3h de los próximos 5 días
        texto = 'Ciudad no encontrada'

        ciudad_id = self._obtener_id( ciudad_pais )
        if ciudad_id != None:
            if type( ciudad_id ) == int:
                mgr = self.owm.weather_manager()
                try:
                    f = mgr.forecast_at_id( ciudad_id, '3h' ).forecast
                except:
                    # Si hay error es porque ciudad_id no corresponde con ninguna ciudad
                    ciudad_id = None

                if ciudad_id != None:
                    texto = 'Pronóstico para ' + self._str_ciudad( ciudad_id ) + '\n'
                    for w in f:
                        strfecha = w.reference_time( 'iso' ).split( '+' )[0]
                        fecha = datetime.strptime( strfecha, '%Y-%m-%d %H:%M:%S' ) 
                        fecha = a_zona( fecha )
                        strfecha = fecha.strftime( '%d/%m/%y %H:%M' )
                        cadena = '\n' + strfecha
                        cadena = cadena + ' ' + w.detailed_status
                        cadena = cadena + ' (' + str( round( w.temperature( 'celsius' )['temp'] ) ) + 'ºC'
                        cadena = cadena + ', ' + str( round( w.wind()['speed'] * 3.6, 1 ) ) + 'km/h'
                        if len( w.rain ) > 0:
                            cadena = cadena + ', ' + str( w.rain['3h'] ) + 'mm/3h'
                        if len( w.snow ) > 0:
                            cadena = cadena + ', ' + str( w.snow['3h'] ) + 'mm/3h'
                        cadena = cadena + ')'

                        texto = texto + cadena
            else:
                texto = 'Ciudad ambigüa; opciones:\n'
                for desambigua in ciudad_id:
                    texto = texto + '\n' + str( desambigua[0] ) + str( '  ' ) + self._str_ciudad( desambigua[0] )

        salida = texto
        return salida
