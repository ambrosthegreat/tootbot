#!/usr/bin/env python3
#
# toot-bot.py
# Librería para las funciones del bot genérico
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# LIBRERIAS
from fake_tuit import arregla_usuario, obtener_datos, generar_fake 
from frases import buscar_en_fich
from random import randint
from re import sub as sed
from requests import get as trae_url
from sys import exc_info as ult_error
from cv2 import imwrite
import social

# CONSTANTES
# LISTA_JULIA = [ 'AmbrosTheGreat@hispatodon.club', 'ivlia_@mastodon.social' ]
LISTA_JULIA = [ 'ivlia_@mastodon.social' ]
NUMTUITS = 10
MAX_CAR = 500
PRENITTER = 'https://nitter.net/'

# FUNCIONES
class Destinatario:
    def __init__( self ):
        self._destinatario = ''

    def buscar( self, texto ):
        salida = ''
        inicio = texto.find( '<a>' )
        if inicio > 0:
            inicio = inicio + 3
            cadena = texto[ inicio : len( texto ) ].strip()
            cadena = cadena[ 0 : len( cadena ) ]
            if cadena[ 0 ] == '<':
                fin = cadena.find( '>' )
                if fin > 1:
                    salida = cadena[ 1 : fin ]
        self._destinatario = salida

    def existe( self ):
        salida = False
        if self._destinatario != '':
            salida = True
        return salida

    def valor( self ):
        return( self._destinatario )

def lista_bots( masto, notificacion ):
    mensaje = '@' + masto.solicitante_notif( notificacion ) +'\n'
    mensaje = mensaje + 'Listado de mis bots'
    mensaje = mensaje + '\n---\n'
    mensaje = mensaje + '@insultos@botsin.space             Bot de insultos\n'
    mensaje = mensaje + '@ChuckNorris_ESP@botsin.space      Bot con frases sobre Chuck Norris\n'
    mensaje = mensaje + '@Mariano_Rajoy_bot@botsin.space    Bot con frases de Mariano Rajoy\n'
    mensaje = mensaje + '@chistes_ES@botsin.space           Bot con chistes\n'
    mensaje = mensaje + '@RAE_no_oficial@botsin.space       Bot con la palabra del día y búsquedas en el DRAE\n'
    mensaje = mensaje + '@tootear@botsin.space              Yo mismo'
    mensaje = mensaje + '@amb_noticias@botsin.space         Bot de noticias'
    masto.toot_texto( mensaje, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def ayuda( masto, notificacion ):
    mensaje = '@' + masto.solicitante_notif( notificacion ) +'\n'
    mensaje = mensaje + 'Bot de proceso de mensajes. Opciones:'
    mensaje = mensaje + '<a> Añadir después <> y sin @ inicial\n'
    mensaje = mensaje + '<b> Listado mis bots\n'
    mensaje = mensaje + '<f> Pronóstico tiempo próximas horas en ciudad a continuación\n'
    mensaje = mensaje + '<ff> Pronóstico tiempo 5 días en ciudad marcada a continuación\n'
    mensaje = mensaje + '<h> Esta ayuda\n'
    mensaje = mensaje + '<i> Recibir un insulto\n'
    mensaje = mensaje + '<j> Fake-tuit <ufrtc>\n'
    mensaje = mensaje + '<p> Recibir un piropo\n'
    mensaje = mensaje + '<t> Tootear lo que haya a continuación\n'
    mensaje = mensaje + '<y> Mensaje al azar de cuenta de historia\n'
    mensaje = mensaje + '<w> El tiempo en ciudad a continuación'
    masto.toot_texto( mensaje, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def run_frases( masto, notificacion, fich_ins, destinatario ):
    solicitante = masto.solicitante_notif( notificacion )
    cabecera = ''
    if destinatario == '':
        cabecera = '@' + solicitante + '\n'
    else:
        cabecera = '@' + destinatario + ' , @' + solicitante + ' quiere decirte algo:\n'

    respuesta = buscar_en_fich( fich_ins, cabecera )
    respuesta = masto.citados( notificacion, respuesta )

    if len( respuesta ) >= masto.masto_max():
        respuesta = "Ha habido un error @" + solicitante + "\nPor favor, vuelve a intentarlo."

    masto.toot_texto( respuesta, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def enviar_mensaje( masto, notificacion , destinatario, id_mensaje ):
    cuerpo = masto.cuerpo_notif( notificacion )
    inicio = cuerpo.find( '<t>' ) + 3
    texto_mensaje = cuerpo[ inicio : len( cuerpo ) ]
    solicitante = masto.solicitante_notif( notificacion )
    if destinatario == '':
        texto_mensaje = '@' + solicitante + ' dice:\n' + texto_mensaje
    else:
        texto_mensaje = '@' + destinatario + ' , @' + solicitante + ' quiere decirte algo:\n' + texto_mensaje

    fotos = notificacion['status']['media_attachments']
    masto.toot_mensaje( texto_mensaje, True, fotos )

def mensaje_historia( masto, notificacion , destinatario, f_lista_hist ):
    solicitante = masto.solicitante_notif( notificacion )

    lista_cuentas = []
    f = open ( f_lista_hist, 'r' )
    lista_cuentas = f.readlines()
    f.close()
    n_cuenta = randint( 0, len( lista_cuentas ) - 1 )
    cuenta = PRENITTER + lista_cuentas[n_cuenta].strip()
    print( 'mensaje_historia => cuenta = ', cuenta )

    tuits = social.getTweets( cuenta, 'hist2mast', NUMTUITS )
    if tuits:
        num_tuits = len( tuits )
        if num_tuits > 0:
            n_tuit = randint( 0, num_tuits - 1 )
            tuit = tuits[n_tuit]
    
        if destinatario == '':
            cabecera = '@' + solicitante + '\n'
        else:
            cabecera = '@' + destinatario + ' , @' + solicitante + ' te envía:\n'

        social.toot_sc( tuit, masto, notificacion, cabecera )
    else:
        respuesta = '@' + solicitante + ' Ha habido un ERROR; vuelve a intenarlo'
        masto.toot_texto( respuesta, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def mensaje_clima( masto, notificacion , destinatario, el_clima ):
    try:
        cuerpo = masto.cuerpo_notif( notificacion ) 
        inicio = cuerpo.find( '<w>' ) + 3
        ciudad = cuerpo[ inicio : len( cuerpo ) ]
        ciudad = ciudad.strip()
        print( 'mensaje_clima() --> ciudad: ' + str( ciudad ) )
    
        clima = el_clima.tiempo_ahora( ciudad )
        texto = clima['texto']
        
        solicitante = masto.solicitante_notif( notificacion )
        if destinatario == '':
            cabecera = '@' + solicitante + '\n'
        else:
            cabecera = '@' + destinatario + ' , @' + solicitante + ' te envía:\n'
        texto = cabecera + texto
        
        img_local = trae_url( clima['url_icono'] ).content
        foto = masto.mastodon.media_post( img_local, mime_type = 'image/png' )
        fotos = []
        fotos.append( foto )

        masto.mastodon.status_post( texto, in_reply_to_id = masto.id_notif( notificacion), visibility = masto.visibilidad_notif( notificacion ), media_ids = fotos )

    except:
        print( 'mensaje_clima() --> Error ' + str( ult_error()[0] ) )
        texto = 'Error ' + str( ult_error()[0] )
        masto.toot_texto( texto, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def mensaje_pronostico( masto, notificacion , destinatario, el_clima ):
    try:
        cuerpo = masto.cuerpo_notif( notificacion )
        inicio = cuerpo.find( '<f>' ) + 3
        ciudad = cuerpo[ inicio : len( cuerpo ) ]
        ciudad = ciudad.strip()
        print( 'mensaje_pronostico() --> ciudad: ' + str( ciudad ) )

        texto = el_clima.pronostico( ciudad )

        solicitante = masto.solicitante_notif( notificacion )
        if destinatario == '':
            cabecera = '@' + solicitante + '\n'
        else:
            cabecera = '@' + destinatario + ' , @' + solicitante + ' te envía:\n'
        texto = cabecera + texto
        if len( texto ) > MAX_CAR:
            texto = texto[ 0 : 500 ]
            lineas = texto.splitlines()
            texto = ''
            for i in range( len( lineas ) - 1 ):
                texto = texto + lineas[i] + '\n'
            texto = texto[ 0 : len( texto ) - 1 ]

        masto.toot_texto( texto, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )
    except:
        print( 'mensaje_pronostico() --> Error ' + str( ult_error()[0] ) )
        texto = 'Error ' + str( ult_error()[0] )
        masto.toot_texto( texto, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def acortar( texto ):
    if len( texto ) > MAX_CAR:
        texto = texto[ 0 : 500 ]
        lineas = texto.splitlines()
        texto = ''
        for i in range( len( lineas ) - 1 ):
            texto = texto + lineas[i] + '\n'
        texto = texto[ 0 : len( texto ) - 1 ]
    return texto

def varios_mensajes( texto, cabecera ):
    mensajes = []
    while len( texto ) > 0:
        mensaje = acortar( texto )
        mensajes.append( mensaje )
        texto = texto[ len( mensaje ) : len( texto ) ]
        if len( texto ) > 0:
            texto = cabecera + texto
    return mensajes

def pronostico_en_hilo( masto, notificacion , destinatario, el_clima ):
    try:
        cuerpo = masto.cuerpo_notif( notificacion )
        inicio = cuerpo.find( '<ff>' ) + 4
        ciudad = cuerpo[ inicio : len( cuerpo ) ]
        ciudad = ciudad.strip()
        print( 'mensaje_pronostico() --> ciudad: ' + str( ciudad ) )

        texto = el_clima.pronostico( ciudad )

        solicitante = masto.solicitante_notif( notificacion )
        if destinatario == '':
            cabecera = '@' + solicitante + '\n'
        else:
            cabecera = '@' + destinatario + ' , @' + solicitante + ' te envía:\n'
        texto = cabecera + texto
        textos = []
        if len( texto ) > MAX_CAR:
            textos = varios_mensajes( texto, cabecera )
        else:
            textos.append( texto )
        
        mi_id = masto.id_notif( notificacion)
        for texto in textos:
            mi_id = masto.mastodon.status_post( texto, in_reply_to_id = mi_id, visibility = masto.visibilidad_notif( notificacion ) )
            print( '---------------------------\nTEXTO:\n', texto )
        
    except:
        print( 'mensaje_pronostico() --> Error ' + str( ult_error()[0] ) )
        texto = 'Error ' + str( ult_error()[0] )
        masto.toot_texto( texto, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def _separar( texto, comando, defecto ):
    # Separa las opciones y, si no existen, asigna el parámetro por defecto
    if texto.find( '<' + comando ) >= 0:
        salida = texto.split( '<' + comando )[1]
        salida = salida.split( '>' )[0]
    else:
        salida = defecto
    return salida.strip()

def fake_tuit( masto, notificacion ):
    # Devuelve un tuit falso
    texto = masto.cuerpo_notif( notificacion )
    texto = texto.replace( '@tootear@botsin.space', '' )
    texto = texto.replace( '@tootear', '' )
    print( texto )

    if texto.find( '<u' ) < 0:
        texto = 'Sin usuario, necesitas ayuda:'
        texto = texto + '\nLa opcióh <j> del bot genera un fake tuit.'
        texto = texto + '\nCuando se le llama, se puede meter lo siguiente dentro del toot:'
        texto = texto + '\n <u usuario> Usuario de Tuiter'
        texto = texto + '\n <f num> Favs'
        texto = texto + '\n <r num> Retuits'
        texto = texto + '\n <t texto> Tiempo'
        texto = texto + '\n <c num> Comentarios'
        texto = texto + '\nEl texto se pasa en el cuerpo del toot.'
        usuario = 'MileyCyrus'
        datos = obtener_datos( usuario )
        datos['usuario'] = '@tootear'
        datos['nombre'] = 'tootbot'
        favs = 10000
        retuits = 10000
        tiempo = 'ahora'
        comentarios = 10000
        cuerpo = texto
    else:
        usuario = arregla_usuario( _separar( texto, 'u', 'MileyCyrus' ) )
        datos = obtener_datos( usuario )

        favs = int( _separar( texto, 'f', str( randint( 30, 5000 ) ) ) )
        retuits = int( _separar( texto, 'r', str( randint( 30, 5000 ) ) ) )
        tiempo = _separar( texto, 't', str( randint( 5, 59 ) ) + 'min' )
        comentarios = int( _separar( texto, 'c', str( randint( 30, 5000 ) ) ) )

        cuerpo = sed( r'<[^<>]*>', '', texto ).strip()

    adjuntos = masto.adjuntos( masto.id_notif( notificacion) )
    if len( adjuntos ) <= 0:
        media = None
    else:
        # Media type: 'image', 'video', 'gifv', 'audio' or 'unknown'
        if adjuntos[0]['type'] == 'image':
            nom_imagen = 'imagen_local_dentro_fake_tmp' + '.' + adjuntos[0]['url'].split( '.' )[-1]
            imagen = trae_url( adjuntos[0]['url'] ).content
            with open( nom_imagen, 'wb' ) as handler:
                handler.write( imagen )
            media = nom_imagen

    # generar_fake( usuario, tiempo, cuerpo, coments = 0, favs = 0, retuits = 0, f_media = None ):
    img = generar_fake( datos, tiempo, cuerpo, comentarios, favs, retuits, media )
    nom_fich = 'fake_tuit_tmp.png'
    imwrite( nom_fich, img )

    cuerpo_toot = '@' + masto.solicitante_notif( notificacion ) + '\n' + 'Impresionante el tuit de '
    cuerpo_toot = cuerpo_toot + datos['nombre'] + '. Lo ha borrado pero antes había hecho una captura de pantalla. La adjunto.'

    foto = masto.mastodon.media_post( nom_fich, mime_type = 'image/png' )
    fotos = []
    fotos.append( foto )

    masto.mastodon.status_post( cuerpo_toot, in_reply_to_id = masto.id_notif( notificacion), visibility = masto.visibilidad_notif( notificacion ), media_ids = fotos )
    print( 'Enviado faketuit:', cuerpo )

def sos_julia( masto, notif, f_julia ):
    # Devuelve un mensaje a Julia para que no responda al inombrable
    solicitante = masto.solicitante_notif( notif )
    mi_id = masto.id_notif( notif )
    cabecera = '@' + solicitante + '\nNI SE TE OCURRA ESCRIBIRLE\n\n'
    texto = buscar_en_fich( f_julia, cabecera )
    masto.toot_texto( texto, mi_id, 'public' )

def comandos( masto, notificacion, nf_insultos, nf_piropos, el_clima, f_lista_hist, f_julia, id_mensaje = '' ):
    # Comandos: a, f, ff, h, i, p, t, r, w
    minus = masto.cuerpo_notif( notificacion ).lower()

    destinatario = ''
    if minus.find( '<a>' )>0:
        print( 'tootbot - comandos => TEXTO = ', minus )
        dest = Destinatario()
        dest.buscar( minus )
        destinatario = dest.valor()
        print( 'tootbot - comandos => Comando <a> --- Destinatario = ', destinatario )

    if minus.find( '<b>' )>0:
        print( 'tootbot - comandos => Comando <b>' )
        lista_bots( masto, notificacion )
    elif minus.find( '<f>' )>0:
        print( 'tootbot - comandos => Comando <f>' )
        mensaje_pronostico( masto, notificacion, destinatario, el_clima )
    elif minus.find( '<ff>' )>0:
        print( 'tootbot - comandos => Comando <ff>' )
        pronostico_en_hilo( masto, notificacion, destinatario, el_clima )
    elif minus.find( '<h>' )>0:
        print( 'tootbot - comandos => Comando <h>' )
        ayuda( masto, notificacion )
    elif minus.find( '<i>' )>0:
        print( 'tootbot - comandos => Comando <i>' )
        run_frases( masto, notificacion, nf_insultos, destinatario )
    elif minus.find( '<j>' )>0:
        print( 'tootbot - comandos => Comando <j>' )
        fake_tuit( masto, notificacion )
    elif minus.find( '<p>' )>0:
        print( 'tootbot - comandos => Comando <p>' )
        run_frases( masto, notificacion, nf_piropos, destinatario )
    elif masto.cuerpo_notif( notificacion ).find( 'S.O.S.' )>0:
        if masto.cuenta_notif( notificacion ) in LISTA_JULIA:
            sos_julia( masto, notificacion, f_julia )
    elif minus.find( '<t>' )>0:
        print( 'tootbot - comandos => Comando <t>' )
        enviar_mensaje( masto, notificacion, destinatario, id_mensaje )
    elif minus.find( '<y>' )>0:
        print( 'tootbot - comandos => Comando <y>' )
        mensaje_historia( masto, notificacion, destinatario, f_lista_hist )
    elif minus.find( '<w>' )>0:
        print( 'tootbot - comandos => Comando <w>' )
        mensaje_clima( masto, notificacion, destinatario, el_clima )
    else:
        print( 'tootbot - comandos => No hay comandos' )
    return
