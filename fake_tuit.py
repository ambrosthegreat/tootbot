#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# fake-tuit
# Generar una imagen con un tuit falso
#

# CONSTANTES
PATH_IMGS = 'fake-tuit_img_base/'
PATH_FUENTES = 'fuentes/'
# Tamaño en píxeles del cuadro para el texto
W_TEXTO = 500
H_TEXTO = 160
COL_FONDO = ( 43.2, 32.4, 19.6 ) # (B, G, R)
COL_TEXTO = ( 255, 255, 255, 0 )
COL_USER = ( 149, 137, 117, 0 )
FUENTE_USUARIO = 'Roboto-Bold.ttf'
FUENTE_TEXTO = 'Roboto-Regular.ttf'
TAMANO_USUARIO = 20
TAMANO_TEXTO = 18
TWITTTER_MAX = 280
LINEA = 60
# Rectángulo superior
SUM_ALTURA = 0
ANCHO = 610
# Contorno
L_CONTORNO = 6
COL_CONTORNO = ( 77, 68, 55 )
# Númericos
TAMANO_NUM = 14
ALTO_NUM = 45
ANCHO_NUM = 100
# Foto de perfil
L_PERFIL = 50
X_PERFIL = 20
Y_PERFIL = 20
# Foto pasada
X_FOTO = 520 - 2 #510
Y_FOTO = 300 - 2 #290
# Punto para separar el tiempo
STR_PUNTO = '·'
# Páginas
URL_NITTER = 'https://nitter.net'
URL_TWITTER = 'https://twitter.com'

# LIBRERÍAS
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from os import getcwd
from PIL import ImageFont, ImageDraw, Image, ImageOps
from requests import get as baja_url
from sys import exit
from textwrap import fill as text_fill
from time import sleep
import numpy as np
import cv2

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Tuitear cosas de Mastodon' )
    parser.add_argument( '-a', '--avatar', help='Foto de perfil', type = str, default = getcwd() + '/' + PATH_IMGS + 'perfil_defecto.jpg' )
    parser.add_argument( '-c', '--comentarios', help='Número de comentarios', type = int, default = 0 )
    parser.add_argument( '-d', '--descargar', help='Descargar datos de cuenta', type=str, required = False )
    parser.add_argument( '-f', '--favs', help='Número de me gusta', type = int, default = 0 )
    parser.add_argument( '-r', '--retuits', help='Número de retuits', type = int, default = 0 )
    parser.add_argument( '-m', '--media', help='Añadir una foto', type = str, required = False )
    parser.add_argument( '-n', '--nombre', help='Nombre completo', type = str, default = 'Miley Ray Cyrus' )
    parser.add_argument( '-p', '--perfil', help='Perfil', type = str, default = '@MileyCyrus' )
    parser.add_argument( '-t', '--tiempo', help='Tiempo desde que se escribió el tuit', type = str, default = '12min' )
    parser.add_argument( '-tm', '--texto', help='Cuerpo del mensaje', type = str, default = 'Hi' )
    parser.add_argument( '-v', '--verificada', help='Cuenta verificada o no', type = bool, default = True )
    return parser.parse_args()

def _formatear( cadena ):
    # Formatear la cadena de texto para que quepa en el espacio reservado
    if len( cadena ) > TWITTTER_MAX:
        salida = cadena[ 0 : TWITTTER_MAX - 1 ]
    else:
        salida = cadena
    salida = text_fill( salida, width = LINEA )
    return salida

def _texto( img_pil, fuente, tamano, x_texto, y_texto, color, texto ):
    # Escribir texto en la imagen
    # Devuelve el tamaño del texto
    fontpath = PATH_FUENTES + fuente
    font = ImageFont.truetype( fontpath, tamano )
    draw = ImageDraw.Draw( img_pil )
    draw.text( ( x_texto, y_texto ), texto, font = font, fill = color )

    return font.getsize( texto )

def _cuadro_cuenta( fondo_pil, pos_x, numero ):
    # Genera un rectángulo para los números de me gusta, retuits o comentarios
    ancho, alto = fondo_pil.size
    cuadro = np.zeros( ( ALTO_NUM, ANCHO_NUM, 3 ), np.uint8 )
    cuadro[:,0:W_TEXTO] = COL_FONDO
    cuadro_pil = Image.fromarray( cuadro )

    if numero != 0:
        str_num = format( numero, ',' )
        str_num = str_num.replace( ',', '.' )
        _texto( cuadro_pil, FUENTE_USUARIO, TAMANO_NUM, 0, 11, COL_USER, str_num )

    fondo_pil.paste( cuadro_pil, ( pos_x, alto - ALTO_NUM ) )

def obtener_datos( perfil ):
    # Obtiene un perfil de Tuitter
    if perfil[0] == '@':
        perfil = perfil[ 1 : len( perfil ) ]
    url_perfil = URL_NITTER + '/' + perfil
    pagina = baja_url( url_perfil ).content
    bsObj = BeautifulSoup( pagina, 'html.parser' )

    # <a class="profile-card-avatar" href="/pic/profile_images%2F1291413039552618498%2FLZfGmhQ_.jpg" target="_blank"><img src="/pic/profile_images%2F1291413039552618498%2FLZfGmhQ__400x400.jpg" alt=""></a>
    # <a class="profile-card-avatar" href="/pic/profile_images%2F1286688674638376962%2FYFeLlxte.jpg" target="_blank"><img src="/pic/profile_images%2F1286688674638376962%2FYFeLlxte_400x400.jpg" alt=""></a>
    avatar = bsObj.find( 'a', attrs = { 'class' : 'profile-card-avatar' } ).get( 'href' ) # .get_text()
    avatar = URL_NITTER + avatar
    
    # <a class="profile-card-fullname" href="/MileyCyrus" title="Miley Ray Cyrus">Miley Ray Cyrus<div class="icon-container"><span class="icon-ok verified-icon" title="Verified account"></span></div></a>
    nombre = bsObj.find( 'a', attrs = { 'class' : 'profile-card-fullname' } ).get( 'title' )
    if bsObj.find( 'span', attrs = { 'class' : 'icon-ok verified-icon' } ) != None:
        verificado = True
    else:
        verificado = False

    salida = { 'usuario':perfil, 'url':url_perfil, 'avatar':avatar, 'nombre':nombre, 'verificado':verificado }
    return salida

def arregla_usuario( usuario ):
    # Si el usuario no empieza por arroba se la pone
    if len( usuario ) > 0:
        if usuario[0] != '@':
            usuario = '@' + usuario
    return usuario

def generar_fake( usuario, tiempo, cuerpo, coments = 0, favs = 0, retuits = 0, f_media = None ):
    # Cuerpo del tuit
    cuerpo = _formatear( cuerpo )
    altura_grande = 500
    
    # Rectángulo del texto
    img = np.zeros( ( altura_grande, W_TEXTO, 3 ), np.uint8 )
    img[:,0:W_TEXTO] = COL_FONDO
    
    # Escribir texto
    img_pil = Image.fromarray( img )
    largo_txt, _ = _texto( img_pil, FUENTE_USUARIO, TAMANO_USUARIO, 0, 0, COL_TEXTO, usuario['nombre'] )
    sumar_x = 0
    if usuario['verificado']:
        img_ver = cv2.imread( PATH_IMGS + 'verificada.png' )
        img_ver_pil = Image.fromarray( img_ver )
        img_pil.paste( img_ver_pil, ( largo_txt + 3, 5 ) )
        sumar_x, _ = img_ver_pil.size
    _texto( img_pil, FUENTE_TEXTO, TAMANO_TEXTO, largo_txt + 6 + sumar_x, 2, COL_USER, usuario['usuario'] + ' ' + STR_PUNTO + ' ' + tiempo )
    _, alto_txt = _texto( img_pil, FUENTE_TEXTO, TAMANO_TEXTO, 0, 25, COL_TEXTO, cuerpo )
    altura = 50 + alto_txt * ( 1+ cuerpo.count( '\n' ) )
    
    # Recortar imagen al texto
    borde = ( 0, 0, 0, altura_grande - altura ) # left, up, right, bottom
    img_pil = ImageOps.crop( img_pil, borde )
    
    # Generar fondo superior
    img_sup = np.zeros( ( altura + SUM_ALTURA , ANCHO, 3 ), np.uint8 )
    img_sup[:,0:ANCHO] = COL_FONDO
    img_sup_pil = Image.fromarray( img_sup )
    img_sup_pil.paste( img_pil, ( 80, 10 ) )
    
    # Fondo inferior
    if f_media != None:
        img_inf = cv2.imread( PATH_IMGS + 'inferior_foto.png' )
    else:
        img_inf = cv2.imread( PATH_IMGS + 'inferior.png' )
    img_inf_pil = Image.fromarray( img_inf )
    ancho_inf, alto_inf = img_inf_pil.size
    
    # Juntar todo
    ancho_sup, alto_sup = img_sup_pil.size
    img = np.zeros( ( alto_inf + alto_sup, ancho_inf, 3 ), np.uint8 )
    img[:,0:W_TEXTO] = COL_FONDO
    img_pil = Image.fromarray( img )
    img_pil.paste( img_sup_pil, ( 0, 0 ) )
    img_pil.paste( img_inf_pil, ( 0, alto_sup ) )
    
    # Tick opciones
    img_tick = cv2.imread( PATH_IMGS + 'tick_opciones.png' )
    img_tick_pil = Image.fromarray( img_tick )
    ancho_tick, _ = img_tick_pil.size
    img_pil.paste( img_tick_pil, ( ancho_sup - ancho_tick , 0 ) )
    
    # Me gusta, retuits, comentarios
    _cuadro_cuenta( img_pil, 110, coments )
    _cuadro_cuenta( img_pil, 245, retuits )
    _cuadro_cuenta( img_pil, 380, favs )
    
    # Foto de perfil
    if usuario['avatar'][0:4] == 'http':
        """
        avatar_local = baja_url( usuario['avatar'], stream=True).raw
        perfil_pil = Image.open( avatar_local )
        """
        avatar_local = baja_url( usuario['avatar'], stream=True).content
        nomfich = 'fake-tuit_avatar_tmp.jpg'
        with open( nomfich, 'wb' ) as handler:
            handler.write( avatar_local )
            handler.close()
    else:
        nomfich = usuario['avatar']

    perfil = cv2.imread( nomfich )
    perfil_pil = Image.fromarray( perfil )

    ancho, alto = perfil_pil.size
    if alto >= ancho:
        alto = round( alto * ( L_PERFIL / ancho ) )
        ancho = L_PERFIL
        perfil_pil = perfil_pil.resize( ( ancho, alto ), Image.ANTIALIAS )
    
        if ancho != alto:
            quitar = round( ( alto - ancho ) / 2 )
            borde = ( 0, quitar, 0, quitar ) # left, up, right, bottom
            perfil_pil = ImageOps.crop( perfil_pil, borde )
    else:
        ancho = round( ancho * ( L_PERFIL / alto ) )
        alto = L_PERFIL
        perfil_pil = perfil_pil.resize( ( ancho, alto ), Image.ANTIALIAS )
    
        if ancho != alto:
            quitar = ancho - alto
            borde = ( quitar, 0, quitar, 0 ) # left, up, right, bottom
            ImageOps.crop( perfil_pil, borde )
    # Dejo la foto redonda
    ancho, alto = perfil_pil.size
    ancho_linea = 20
    radio = round( L_PERFIL / 2 + 1 + ancho_linea / 2 )
    centro = round( ancho / 2 )
    perfil = np.array( perfil_pil )
    perfil = cv2.circle( perfil, ( centro, centro ), radio, COL_FONDO, ancho_linea )
    perfil_pil = Image.fromarray( perfil )
    img_pil.paste( perfil_pil, ( X_PERFIL, Y_PERFIL ) )
    
    # Imagen añadida
    if f_media != None:
        # Leer y redimensionar foto
        media = cv2.imread( f_media )
        media_pil = Image.fromarray( media )
        ancho_media, alto_media = media_pil.size
        prop_x = ancho_media / X_FOTO
        prop_y = alto_media / Y_FOTO
        if prop_y > prop_x:
            alto_media = round( alto_media * ( X_FOTO / ancho_media ) )
            ancho_media = X_FOTO
            media_pil = media_pil.resize( ( ancho_media, alto_media ), Image.ANTIALIAS )
            if ancho_media != alto_media:
                quitar = abs( round( ( alto_media - Y_FOTO ) / 2 ) )
                borde = ( 0, quitar, 0, quitar ) # left, up, right, bottom
                media_pil = ImageOps.crop( media_pil, borde )
        else:
            ancho_media = round( ancho_media * ( Y_FOTO / alto_media ) )
            alto_media = Y_FOTO
            media_pil = media_pil.resize( ( ancho_media, alto_media ), Image.ANTIALIAS )
            if ancho_media != alto_media:
                quitar = abs( round( ( ancho_media - X_FOTO ) / 2 ) )
                borde = ( quitar, 0, quitar, 0 ) # left, up, right, bottom
                media_pil = ImageOps.crop( media_pil, borde )
        
        # Pegar en la captura
        marco_pil = Image.open( PATH_IMGS + 'marco.png' )
        final = Image.new( 'RGBA', media_pil.size, ( 0, 0, 0, 0 ) )
        final.paste( media_pil, ( 0, 0 ) )
        final.paste( marco_pil, ( 0, 0 ), mask = marco_pil )
        media_pil = final
    
        ancho, alto = img_pil.size
        pos_x = 70
        pos_y = alto - ( alto_media + ALTO_NUM )
        img_pil.paste( media_pil, ( pos_x, pos_y ) )
    
    # Dibujar contorno
    img = np.array( img_pil )
    alto = alto_inf + alto_sup
    img = cv2.line( img, ( L_CONTORNO, 0 ), ( L_CONTORNO, alto ), COL_CONTORNO, 1)
    img = cv2.line( img, ( ancho_inf - L_CONTORNO, 0 ), ( ancho_inf - L_CONTORNO, alto ), COL_CONTORNO, 1)
    img = cv2.line( img, ( L_CONTORNO, L_CONTORNO ), ( ancho_inf - L_CONTORNO, L_CONTORNO ), COL_CONTORNO, 1)
    img = cv2.line( img, ( L_CONTORNO, alto - L_CONTORNO ), ( ancho_inf - L_CONTORNO, alto - L_CONTORNO ), COL_CONTORNO, 1)

    return img
