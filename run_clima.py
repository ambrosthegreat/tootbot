#!/usr/bin/env python3
#
# run_clima
# Obtener en modo local para pruebas el clima en una localidad
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES

# LIBRERIAS
from argparse import ArgumentParser
from mi_clima import Mi_clima
from os import getcwd

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Corre localmente el módulo para el clima' )
    parser.add_argument( '-c', '--ciudad', help = 'Ciudad a buscar', type = str, required = True )
    parser.add_argument( '-w', '--weather_token', help = 'Token de OpenWeatherMap', type = str, required = True )
    return parser.parse_args()

def mensaje_clima( el_clima, ciudad ):
    print( 'mensaje_clima() --> ciudad: ' + str( ciudad ) )
    clima = el_clima.tiempo_ahora( ciudad )
    print( clima['texto'] )
    print( 'ICONO = ' + clima['url_icono'] )

# MAIN
args = _args()

el_clima = Mi_clima( args.weather_token )
mensaje_clima( el_clima, args.ciudad )
